from flask import render_template, flash, redirect, url_for, request, jsonify
from app import app, CORS


@app.route("/", methods=["GET", "POST"])
def home():
    if request.method == "POST":
        email = request.form.get("email")
        password = request.form.get("password")
        print(email, password)
    return render_template('index.html')
