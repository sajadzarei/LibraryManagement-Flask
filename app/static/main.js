// Login Modal
const modalBtn = document.querySelector("#loginBtn");
const modal = document.querySelector("#simpleModal");
const closeBtn = document.querySelector(".closeBtn");

modalBtn.addEventListener("click", openModal);
closeBtn.addEventListener("click", closeModal);
window.addEventListener("click", clickOutside);

function openModal() {
  modal.style.display = "block";
}
function closeModal() {
  modal.style.display = "none";
}
function clickOutside(e) {
  if (e.target === modal) {
    modal.style.display = "none";
  }
}

// Sending Login form data to flask
const loginForm = document.querySelector("#loginForm");

loginForm.addEventListener("submit", (e) => {
  e.preventDefault();
  const inputEmail = document.querySelector("#inputEmail").value;
  const inputPassword = document.querySelector("#inputPassword").value;
  // Show message if fields are empty
  if (inputEmail !== "" && inputPassword !== "") {
    const xml = new XMLHttpRequest();
    const data = new FormData();
    // add email and password value
    data.append("email", inputEmail);
    data.append("password", inputPassword);

    xml.open("POST", "http://127.0.0.1:5000/", true);
    // xml.onload = function (e) {
    //   if (xml.status === 200) {
    //     // console.log(xml.status);
    //   } else {
    //     document.querySelector(".login-form-message").innerHTML =
    //       "Something went wrong!";
    //     document.querySelector(".login-form-message").style.display = "block";
    //     // console.error(xml.status);
    //   }
    // };
    xml.send(data);
    // Clear fields
    document.querySelector("#inputEmail").value = "";
    document.querySelector("#inputPassword").value = "";
  } else {
    document.querySelector(".login-form-message").style.display = "block";
  }
});
